from NeuralNetwork import NeuralNetwork

network = {
    "name": "Sin test",
    "network": {
        "layers": [
            {
                "type": "Dense",
                "units": 10,
                "activation": "sigmoid",
                "input_shape": (1,)
            },
            {
                "type": "Dense",
                "units": 1
            }
        ],
        "compile": {
            "loss": "mean_squared_error",
            "optimizer": {
                "type": "SGD"
            },
            "metrics": [
                'accuracy',
                'mean_squared_error'
            ]
        }
    }
}

ann = NeuralNetwork(network)

ann.create()

import numpy as np
x_train = np.linspace(-np.pi, np.pi, 100)
y_train = np.sin(x_train)

print(x_train)
print(np.shape(x_train))
print(y_train)
print(np.shape(y_train))

ann.train(x_train, y_train, 100000)

x_test = np.linspace(-1, 1, 100)
y_test = np.sin(x_test)

score = ann.evaluate(x_test, y_test)
print(score)

x = np.linspace(-2*np.pi, 4*np.pi, 10000)

preds = ann.predict(x)

import matplotlib.pyplot as plot
plot.plot(x_train, y_train)
plot.plot(x, preds)
plot.show()
