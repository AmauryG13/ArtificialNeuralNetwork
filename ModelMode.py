from keras.models import Sequential

from Abstract import ModelPragma

from Class import LayerParser

class ModelMode(ModelPragma, LayerParser):
    parser = {
        "layer": LayerParser()
    }

    def __init__(self, structure, options = {}):
        # Public
        self.model = None
        # Protected
        self._method = None
        self._model = None
        self._layers = []
        self._optimizer = []
        # Inheritence
        ModelPragma.__init__(self, structure)
        self._init(structure)

    def createModel(self):
        self._initModel()
        self._initLayers()
        self._initOptimizer()
        self._initCompile()
        self.model = self._model

    def _init(self, structure):
        if "model" in structure.keys():
            self._method = "loader"

        self._method = "parser"
        ModelMode.parser["layer"].init(structure)

    def _initModel(self):
        self._model = Sequential()

    def _initLayers(self):
        self._layers = ModelMode.parser["layer"].createLayers()

        for layer in self._layers:
            self._model.add(layer)

        self.model = self._model

    def _initOptimizer(self):
        self._optimizer = ModelMode.parser["layer"].createOptimizer()

    def _initCompile(self):
        compil = self.__convert(ModelMode.parser["layer"].createCompile())
        self.model = self._model.compile(loss=compil["loss"], optimizer=compil["optimizer"], **compil["args"])

    def __convert(self, output):
        out = {"args": {}}

        args = ["loss", "optimizer"]
        for key in output.keys():
            if key in args:
                out.update({key: output[key]})
            else:
                out["args"].update({key: output[key]})

        return out




if __name__ == '__main__':
    pass
