from abc import ABC, abstractmethod

class AbstractLoader(ABC):
    @abstractmethod
    def _import(self, class_):
        pass

    @abstractmethod
    def instantiate(self, row):
        pass

class LayerLoader(AbstractLoader):
    def __init__(self):
        self.module = 'keras.layers'
        self._module = ""

    def _import(self, class_):
        self._module = __import__(self.module, fromlist=[class_])

    def instantiate(self, layer):
        self._import(layer["type"])
        class_ = getattr(self._module, layer["type"])

        if not "args" in layer.keys():
            layer.update({"args": {}})

        instance = class_(layer["arg"], **layer["args"])
        return instance

class OptimizerLoader(AbstractLoader):
    def __init__(self):
        self.module = 'keras.optimizers'
        self._module = ""

    def _import(self, class_):
        self._module = __import__(self.module, fromlist=[class_])

    def instantiate(self, optimizer):
        print(optimizer)
        self._import(optimizer["type"])
        class_ = getattr(self._module, optimizer["type"])
        instance = class_(**optimizer["args"])
        return instance

class DynamicLoader():
    def __init__(self, ttype):
        self.type = ttype

        self._layer = LayerLoader()
        self._optimizer = OptimizerLoader()

    def instantiate(self, row):
        loader = getattr(self, '_' + self.type)
        return loader.instantiate(row)
