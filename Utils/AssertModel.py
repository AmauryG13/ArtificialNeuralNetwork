class AssertModel(object):
    def __init__(self):
        self.models = ["MLP", "MLPP"]

    def checkModel(self, structure):
        if "model" in structure.keys():
            return structure["model"]["name"] in self.models

        return False
