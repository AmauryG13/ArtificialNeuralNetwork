from abc import ABC, abstractmethod

class AbstractNetwork(ABC):
    @abstractmethod
    def create(self):
        pass

    @abstractmethod
    def train(self, inputs, outputs):
        pass

    @abstractmethod
    def evaluate(self):
        pass

    @abstractmethod
    def predict(self, inputs):
        pass
