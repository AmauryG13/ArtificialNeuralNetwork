from abc import ABC, abstractmethod

class ModelPragma(ABC):
    def __init__(self, structure, options = {}):
        self.structure = structure
        self._options = options
        super(ModelPragma, self).__init__()

    @abstractmethod
    def _initModel(self):
        pass

    @abstractmethod
    def _initLayers(self):
        pass

    @abstractmethod
    def _initOptimizer(self):
        pass

    @abstractmethod
    def _initCompile(self):
        pass

    @abstractmethod
    def createModel(self):
        pass
