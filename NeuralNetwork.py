from Abstract import AbstractNetwork
from ModelMode import ModelMode

class NeuralNetwork(AbstractNetwork, ModelMode):
    def __init__(self, structure):
        self.batch_size = 128
        # Inheritence
        ModelMode.__init__(self, structure)

    def create(self):
        self.createModel()

    def train(self, x, y, iterations):
        self.model.fit(x, y, epochs=iterations, batch_size=self.batch_size)

    def evaluate(self, x, y):
        score = self.model.evaluate(x, y, batch_size=self.batch_size)
        return score

    def predict(self, x):
        predictions = self.model.predict(x, batch_size=self.batch_size)
        return predictions
