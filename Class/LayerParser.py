from os import getcwd
from json import load

try:
    from Utils import DynamicLoader
    from Utils import AssertModel
except ModuleNotFoundError:
    raise RuntimeError('Submodule from Utils not loaded')

from .InputParser import InputParser
from .ArgumentParser import ArgumentParser

class LayerParser(AssertModel, InputParser, ArgumentParser):

    loader = {
        'layer': DynamicLoader('layer'),
        'optimizer': DynamicLoader('optimizer')
    }

    def __init__(self):
        # Args
        self.structure = []
        # Public class properties
        self.layers = []
        self.optimizer = ""
        self.compile = {}
        # Hidden class properties
        self._structure = {
            "path": "",
            "file": "",
            "data": {}
        }
        self._layers = []
        self._optimizer = "rmsprop"
        self._compile = {}
        # Inheritence
        AssertModel.__init__(self)
        InputParser.__init__(self)
        ArgumentParser.__init__(self)

    def init(self, structure):
        self.structure = structure

        if self.checkModel(structure):
            self.parse(structure["model"]["inputs"])
            self._getJsonFile(structure)
            self._parseJsonFile()
            return self

        if "network" in structure.keys():
            self._parseStructure()
            return self

        raise RuntimeError('Unknown Model')


    def createLayers(self):
        for index, layer in enumerate(self._layers):
            if index == 0:
                layer = self.parseInput(layer)

            self.layers.append(self.loader['layer'].instantiate(self.convert(layer)))
        return self.layers


    def createOptimizer(self):
        self.optimizer = self.loader['optimizer'].instantiate(self.convert(self._optimizer))
        return self.optimizer

    def createCompile(self):
        self.compile = self._compile
        self.compile.update({"optimizer": self.optimizer})
        return self.compile

    def _getJsonFile(self, structure):
        model = structure["model"]
        if not "path" in model.keys():
            model["path"] = getcwd() + '/Class/structure/' + model["name"] + '.json'

        self._structure["path"] = model["path"]
        self._structure["file"] = open(self._structure["path"])
        self._structure["data"] = load(self._structure["file"])

    def _parseJsonFile(self):
        self._layers = self._structure["data"]["network"]["layers"]
        self._optimizer = self._structure["data"]["network"]["compile"]["optimizer"]
        self._compile = self._structure["data"]["network"]["compile"]

    def _parseStructure(self):
        self._layers = self.structure["network"]["layers"]
        self._optimizer = self.structure["network"]["compile"]["optimizer"]
        self._compile = self.structure["network"]["compile"]


if __name__ == '__main__':
    print('[main]' + __main__)
    import sys
    print('[path]' + sys.path)
    sys.path.append(sys.path[0] + '/utils')

    layer = LayerParser()
