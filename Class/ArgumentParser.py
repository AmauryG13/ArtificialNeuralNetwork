class ArgumentParser():
    pargs = {
        "Dense": {
            "arg": ["units"]
        },
        "Dropout": {
            "arg": ["rate"]
        },
        "Activation": {
            "arg": ["activation"]
        },
        "compile": {
            "arg": ["loss", "optimizer"]
        }
    }

    def __init__(self):
        self.output = {
            "arg": {},
            "args": {}
        }

    def convert(self, inputs):
        output = {"type": "", "arg": {}, "args": {}}

        types = ArgumentParser.pargs.keys()

        if inputs["type"] in types:
            for t in types:
                if inputs["type"] == t:
                    output["type"] = t
                    for key in inputs.keys():
                        if not key == "type":
                            if key in ArgumentParser.pargs[t]["arg"]:
                                output["arg"] = inputs[key]
                            else:
                                output["args"].update({key: inputs[key]})
            return output
        else:
            output["type"] = inputs["type"]
            for key in inputs.keys():
                if not key == "type":
                    output["args"].update({key: inputs[key]})
            return output

        raise RuntimeError("Unknown input")

if __name__ == '__main__':
    args = ArgumentParser()
    output = {}

    dense = {
        "type": "Dense",
        "units": 64,
        "activation": "relu"
    }

    dropout = {
        "type": "Dropout",
        "rate": 0.5
    }

    output["dense"] = args.convert(dense)
    output["dropout"] = args.convert(dropout)

    print(output)
