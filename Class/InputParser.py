class InputParser(object):
    args = {
        "name": ["input_shape"]
    }

    def __init__(self):
        self._inputs = ()

    def parse(self, inputs):
        self._inputs = inputs

    def parseInput(self, args):
        key = InputParser.args["name"][0]
        if not key in args.keys():
            args.update({key: self.inputs})
        return args

    @property
    def inputs(self):
        return self._inputs

    @inputs.setter
    def inputs(self, values):
        if type(values) is tuple:
            self._inputs = values
        else:
            raise RuntimeError("Input shape has to be a tuple")
